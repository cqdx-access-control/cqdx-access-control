import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import "@/assets/styles/common.scss";
createApp(App).use(store).use(router).use(Antd).mount("#app");
router.beforeEach((to,from,next)=>{
    store.commit("getTabs");
    store.commit("setActiveTabs",to.path)
    next()
})