const tabs={
    state:{
        //当前激活选项卡
        activeKey:"",
        panes:[
            {
                title: '首页',
                key: '/index',
            },
        ]
    },
    actions:{

    },
    modules:{
    },
    mutations:{
        addTabs(state,val){
            const index=state.panes.findIndex(item=>item.key===val.key)
            if(index===-1){
                state.panes.push(val)
            }
            //设置当前激活选项卡
            state.activeKey=val.key
            //    将当前数据放到缓存中
            sessionStorage.setItem("tabList",JSON.stringify(state.panes))
        },
    //    获取Tabs
        getTabs(state){
            const tabs=sessionStorage.getItem("tabList")
            if (tabs){
                state.panes=JSON.parse(tabs)
            }
        },
        // 设置当前激活的选项卡
        setActiveTabs(state, val) {
            state.activeKey = val;
        },
    },
}
export default tabs
