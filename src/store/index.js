import { createStore } from 'vuex'
import menu from "@/store/menu";
import tabs from "@/store/tabs";
export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    menu,
    tabs
  }
})
