import axios from "axios";
import { ElMessage, ElMessageBox } from "element-plus";
// import router from "../router";
import store from "@/store"
axios.defaults.headers.get["Content-Type"] =
    "application/x-www-form-urlencoded";
// 请求拦截器
axios.interceptors.request.use(
    (config) => {
        axios.defaults.headers.get["Content-Type"] =
            "application/x-www-form-urlencoded";
        //  头部携带token
        // config.headers["Authorization"] = Cookies.get("token");
        return config;
    },
    (error) => {
        Promise.reject(error);
    }
);
// 响应拦截器
axios.interceptors.response.use(
    (res) => {
        // 未设置状态码则默认成功状态
        const code = res.data.code;
        if (code === "401" || code === 401) {
            ElMessageBox.confirm(
                "登录状态已过期，您可以继续留在该页面，或者重新登录",
                "系统提示",
                {
                    confirmButtonText: "重新登录",
                    cancelButtonText: "取消",
                    type: "warning",
                }
            ).then(() => {
                store.dispatch("resetToken").then(() => {
                    location.reload();
                });
            });
        } else if (code === "500") {
            ElMessage.error(res.data.msg);
        } else {
            return res.data;
        }
    },
    (error) => {
        ElMessage.error("请求超时");
        return Promise.reject(error);
    }
);
export default axios;
