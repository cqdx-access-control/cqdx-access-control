import { createRouter, createWebHistory } from "vue-router";
import Index from "../views/Index.vue";
import Layout from "@/views/Layout"
const routes = [
  {
    path:"/",
    name:"index",
    component: Layout,
    children:[
      {
        path:"/communityManagement",
        name:"communityManagement",
        meta:{
          title:"小区管理"
        },
        component:()=>import("@/views/AccessManagement/CommunityManagement/Index")
      },
      {
        path:"/roomSys",
        name:"roomSys",
        component:()=>import("@/views/AccessManagement/ParameterSettings/Index"),
        meta:{
          title:"住宅参数设置"
        },
      },
      {
        path:"/personnelFiling",
        name:"personnelFiling",
        component:()=>import("@/views/AccessManagement/PersonnelFiling/Index"),
        meta:{
          title:"人员备案"
        },
        children:[
          {
            path:"/generalFiling",
            name:"generalFiling",
            component:()=>import("@/views/AccessManagement/PersonnelFiling/GeneralFiling"),
            meta:{
              title:"通用备案"
            },
          },
          {
            path:"/residentFiling",
            name:"residentFiling",
            component:()=>import("@/views/AccessManagement/PersonnelFiling/ResidentFiling"),
            meta:{
              title:"居民备案"
            },
          }
        ]
      }
    ]
  },
  {
    path: "/index",
    name: "Index",
    component: Index,
  },
];
const router = createRouter({
  mode:"history",
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
