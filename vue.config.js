const { defineConfig } = require("@vue/cli-service");
// const path = require("path");
// function resolve(dir) {
//   return path.join(__dirname, dir);
// }
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer:{
    port:8086,
    proxy:{
      "/api": {
        target: "http://47.108.66.148:8087/", //目标地址 外网
        changeOrigin: true, //是否跨域  是true
        ws: true, //加强跨域,
        pathRewrite: {
          "^/api": "" //请求时用api
        }
      }
    }
  }
});
